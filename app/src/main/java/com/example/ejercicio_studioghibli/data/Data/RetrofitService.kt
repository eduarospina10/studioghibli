package com.example.ejercicio_studioghibli.data.Data

import com.example.ejercicio_studioghibli.data.Models.filmItem
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST

interface RetrofitService {
    @GET("films")
    suspend fun getFilms():List<filmItem>
}

object RetrofitClient {
    fun makeRetrofitService(): RetrofitService {
        return Retrofit.Builder()
            .baseUrl("https://ghibliapi.vercel.app/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RetrofitService::class.java)
    }
}
