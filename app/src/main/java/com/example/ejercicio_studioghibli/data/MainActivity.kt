package com.example.ejercicio_studioghibli.data

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.ejercicio_studioghibli.data.Data.RetrofitClient
import com.example.ejercicio_studioghibli.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var ghibiAdapter: GhibiAdapter
    private lateinit var glayout:GridLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        glayout = GridLayoutManager(this, 1)

        binding.RecyclerView.apply {
            layoutManager = glayout
        }

        val service = RetrofitClient.makeRetrofitService()

        lifecycleScope.launch {
            val ghibliFilms=service.getFilms()
            val films = ghibliFilms
            ghibiAdapter = GhibiAdapter(films)
            binding.RecyclerView.adapter = ghibiAdapter
        }

    }
}