package com.example.ejercicio_studioghibli.data

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.ejercicio_studioghibli.R
import com.bumptech.glide.Glide
import com.example.ejercicio_studioghibli.data.Models.filmItem
import com.example.ejercicio_studioghibli.databinding.ItemGhibliBinding

class GhibiAdapter(private val results: List<filmItem>):RecyclerView.Adapter<GhibiAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_ghibli, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = results[position]
        with(holder) {
            binding.GhibliId.text = position.toString()
            binding.GhibliTitle.text = result.title
            binding.GhibliURL.text = result.url
            Glide.with(binding.root)
                .load(result.image)
                .into(binding.GhibliImg)

            val context = binding.root.context


            binding.root.setOnClickListener {
                val url = result.url
                openUrlInBrowser(context, url)
            }
        }
    }
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        val binding = ItemGhibliBinding.bind(view)
    }
}

private fun openUrlInBrowser(context: Context, url: String) {
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
    context.startActivity(intent)
}

